import glob from "glob";
import Vue from "vue";
import { promises as fs } from "fs";
import path from 'path'
import { createRenderer } from "vue-server-renderer";
import sort from "version-sort";

const template = await fs.readFile('./index.template.html', 'utf-8');

const renderer = createRenderer({
	template
});

let files = glob.sync("../public/*.js")

let versions = files.map(a => path.parse(a).name).filter(a => a !== 'latest')

const app = new Vue({
	data: {
		versions: sort(versions).reverse()
	},
	template: `<main>
	<h1>Hosted versions</h1>
    <h2><a href="/latest.js">latest.js</a><time v-text="versions[0]"/></h2>
    <h2 v-for="version in versions"><a :href="'/'+version+'.js'" v-text="version+'.js'" /><time v-text="version" /></h2>
	</main>`,
});

const html = await renderer.renderToString(app)

await fs.writeFile("../public/index.html", html, 'utf-8')

