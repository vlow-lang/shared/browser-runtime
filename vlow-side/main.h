#pragma once

typedef void* vnode;

#ifdef __cplusplus
extern "C" {
#endif

void createComponent(const char* name);
void* createInstanceOf(char* name, void* instancePtr);

void * h(const char* sel, const char *content);
void addChild(void *vnodePtr, void *childPtr);

void addEventHandler(void *vnodePtr, const char* eventPtr, int callback(void *), void *payload);
void addStyle(void *vnodePtr, const char* propPtr, const char* valuePtr);
void addProp(void *vnodePtr, const char* propPtr, const char* valuePtr);

void patch(void *instancePtr, void *vrootPtr);

#ifdef __cplusplus
}
#endif
