#include <stdio.h>
#include <vector>
#include <string>
#include <functional>

// Function defined in loader.js
#include "main.h"
extern "C" void addHandler(void *vnodePtr, const char* eventPtr, int handlerNr);

static std::vector<std::function<int(void)>> handler;
void addEventHandler(void *vnodePtr, const char* eventPtr, int callback(void *), void *payload) {
   handler.push_back([=](){
        return callback(payload);
   });

   addHandler(vnodePtr, eventPtr, handler.size() - 1);
}
extern "C" int handleEvent(int handlerNr) {
    return handler[handlerNr]();
}

// This function is called at startup
int main(int argc, char *argv[]) {
	printf("registering components ...\n");
	createComponent( "haruhi-suzumiya");
	printf("done\n");

	return 0;
}
