import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
	input: 'vlow-component-loader.js',
	output: {
		file: "dist/vlow-component-loader.js",
		name: "vlow-component-loader",
		format: "umd"
	},
	plugins: [
		nodeResolve()
	]
};
