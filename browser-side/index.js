import {
	init,
	classModule,
	propsModule,
	styleModule,
	eventListenersModule,
	toVNode,
	h
} from "snabbdom";

const patch = init([
	// Init patch function with chosen modules
	classModule, // makes it easy to toggle classes
	propsModule, // for setting properties on DOM elements
	styleModule, // handles styling on elements with support for animations
	eventListenersModule, // attaches event listeners
]);

async function loadVlowWasm(wasmBuffer) {

	const module = await WebAssembly.compile(wasmBuffer)

	const vnodeStore = [];
	const componentStore = [];

	let WM,
		ASM,
		MEM,
		MU8,
		MU16,
		MU32,
		MI32,
		MF32;

	const WA = {};

	function createComponent(name) {
		const VlowComponent = class extends HTMLElement {
			constructor() {
				super();

				this._shadowRoot = this.attachShadow({mode: 'open'});

				const newDiv = document.createElement("div");

				this._shadowRoot.appendChild(newDiv);
				this._vRoot = toVNode(newDiv)
			}

			connectedCallback() {
				let instancePtr = componentStore.push(this) - 1;

				let namePtr = MStrPut(name)
				this.instance = ASM.createInstanceOf(namePtr, instancePtr);
				ASM.free(namePtr)
			}

			disconnectedCallback() {

			}
		}

		Object.defineProperty(VlowComponent.prototype, 'label', {
			get: function () {
				return this.getAttribute('label');
			},
			set: function () {
				this.setAttribute('label', value);
			}
		});

		Object.defineProperty(VlowComponent, "observedAttributes", {
			value: ['label']
		})

		Object.defineProperty(VlowComponent.prototype, "attributeChangedCallback", {
			value: function (name, oldVal, newVal) {
				console.log(name, oldVal, newVal);
			}
		})

		window.customElements.define(name, VlowComponent);
	}

	/*
	  WAjic - WebAssembly JavaScript Interface Creator
	  Copyright (C) 2020 Bernhard Schelling

	  This software is provided 'as-is', without any express or implied
	  warranty.  In no event will the authors be held liable for any damages
	  arising from the use of this software.

	  Permission is granted to anyone to use this software for any purpose,
	  including commercial applications, and to alter it and redistribute it
	  freely, subject to the following restrictions:

	  1. The origin of this software must not be misrepresented; you must not
		 claim that you wrote the original software. If you use this software
		 in a product, an acknowledgment in the product documentation would be
		 appreciated but is not required.
	  2. Altered source versions must be plainly marked as such, and must not be
		 misrepresented as being the original software.
	  3. This notice may not be removed or altered from any source distribution.
	*/

	// Define print and error functions if not yet defined by the outer html file
	const print = WA.print = msg => console.log("%c[WASM]", "color: green; background-color: black;", msg.replace(/\n$/, ''));
	const error = WA.error = (code, msg) => console.error("%c[WASM]", "color: green; background-color: black;", code + ': ' + msg + '\n');

	// Some global state variables and max heap definition

	let WASM_HEAP,
		WASM_HEAP_MAX = (WA.maxmem || 256 * 1024 * 1024); //default max 256MB

	// A generic abort function that if called stops the execution of the program and shows an error
	let STOP;
	const abort = WA.abort = function (code, msg) {
		STOP = true;
		error(code, msg);
		throw 'abort';
	};

	// Puts a string from JavaScript onto the wasm memory heap (encoded as UTF8)
	const MStrPut = function (str, ptr, buf_size) {
		if (buf_size === 0) return 0;
		let buf = new TextEncoder().encode(str);
		let bufLen = buf.length;
		let out = (ptr || ASM.malloc(bufLen + 1));
		if (buf_size && bufLen >= buf_size)
			for (bufLen = buf_size - 1; (buf[bufLen] & 0xC0) === 0x80; bufLen--) ;
		MU8.set(buf.subarray(0, bufLen), out);
		MU8[out + bufLen] = 0;
		return (ptr ? bufLen : out);
	};

	// Reads a string from the wasm memory heap to JavaScript (decoded as UTF8)
	const MStrGet = function (ptr, length) {
		if (length === 0 || !ptr) return '';
		if (!length) {
			for (length = 0; length !== ptr + MU8.length && MU8[ptr + length]; length++) ;
		}
		return new TextDecoder().decode(MU8.subarray(ptr, ptr + length));
	};

	// Copy a JavaScript array to the wasm memory heap
	const MArrPut = function (a) {
		const len = a.byteLength || a.length;
		const ptr = len && ASM.malloc(len);

		MU8.set(a, ptr);

		return ptr;
	};

	// Set the array views of various data types used to read/write to the wasm memory from JavaScript
	const MSetViews = function () {
		const buf = MEM.buffer;

		MU8 = new Uint8Array(buf);
		MU16 = new Uint16Array(buf);
		MU32 = new Uint32Array(buf);
		MI32 = new Int32Array(buf);
		MF32 = new Float32Array(buf);
	};

	const emptyFunction = () => 0;
	const crashFunction = (msg) => abort('CRASH', msg);

	// Set up the import objects that contains the functions passed to the wasm module
	const env = {
		// sbrk gets called to increase the size of the memory heap by an increment
		sbrk(increment) {
			var heapOld = WASM_HEAP,
				heapNew = heapOld + increment,
				heapGrow = heapNew - MEM.buffer.byteLength;
			//console.log('[SBRK] Increment: ' + increment + ' - HEAP: ' + heapOld + ' -> ' + heapNew + (heapGrow > 0 ? ' - GROW BY ' + heapGrow + ' (' + (heapGrow>>16) + ' pages)' : ''));
			if (heapNew > WASM_HEAP_MAX) abort('MEM', 'Out of memory');
			if (heapGrow > 0) {
				MEM.grow((heapGrow + 65535) >> 16);
				MSetViews();
			}
			WASM_HEAP = heapNew;
			return heapOld;
		},

		// Functions querying the system time
		time(ptr) {
			var ret = (Date.now() / 1000) | 0;
			if (ptr) MU32[ptr >> 2] = ret;
			return ret;
		},
		gettimeofday(ptr) {
			var now = Date.now();
			MU32[ptr >> 2] = (now / 1000) | 0;
			MU32[(ptr + 4) >> 2] = ((now % 1000) * 1000) | 0;
		},

		// Failed assert will abort the program
		__assert_fail(condition, filename, line, func) {
			crashFunction('assert ' + MStrGet(condition) + ' at: ' + (filename ? MStrGet(filename) : '?'), line, (func ? MStrGet(func) : '?'))
		},

		// custom vlow stuff
		createComponent(name) {
			createComponent(MStrGet(name))
			// 		var b = WA.malloc_array(new Uint8Array(xhr.response));
			// 		WA.asm.free(b);
		},
		h(selectorPtr, contentPtr) {
			let selector = MStrGet(selectorPtr)
			let content = contentPtr ? MStrGet(contentPtr) : undefined
			let vnode = h(selector, {
				on   : {},
				style: {},
				props: {}
			}, content);
			return vnodeStore.push(vnode) - 1
		},
		addChild(vnodePtr, childPtr) {
			let vnode = vnodeStore[vnodePtr];
			let vchild = vnodeStore[childPtr];

			if (vnode.children === undefined) {
				vnode.children = [];
			}

			vnode.children.push(vchild)
		},
		patch(instancePtr, vrootPtr) {
			let component = componentStore[instancePtr];
			let vroot = vnodeStore[vrootPtr];

			component._vRoot = patch(component._vRoot, vroot);
		},

		addHandler(vnodePtr, eventPtr, handlerNr) {
			let vnode = vnodeStore[vnodePtr];
			let event = MStrGet(eventPtr);
			vnode.data.on[event] = () => ASM.handleEvent(handlerNr);
		},

		addStyle(vnodePtr, propPtr, valuePtr) {
			let vnode = vnodeStore[vnodePtr];
			let prop = MStrGet(propPtr);
			let value = MStrGet(valuePtr);

			vnode.data.style[prop] = value;
		},

		addProp(vnodePtr, propPtr, valuePtr) {
			let vnode = vnodeStore[vnodePtr];
			let prop = MStrGet(propPtr);
			let value = MStrGet(valuePtr);

			vnode.data.props[prop] = value;
		}
	};
	const imports = {env};

	// Go through all the imports to fill out the list of functions
	for (const {
		kind,
		module: mod,
		name
	} of WebAssembly.Module.imports(module)) {
		const obj = (imports[mod] || (imports[mod] = {}));

		if (kind === 'memory') {
			// This WASM module wants to import memory from JavaScript
			// The only way to find out how much it wants initially is to parse the module binary stream
			// This code goes through the wasm file sections according the binary encoding description
			//     https://webassembly.org/docs/binary-encoding/
			for (let wasm = new Uint8Array(wasmBuffer), i = 8, iMax = wasm.length, iSectionEnd, type, len, j, Get; i < iMax; i = iSectionEnd) {
				// Get() gets a LEB128 variable-length number optionally skipping some bytes before
				Get = s => {
					i += s | 0;
					for (var b, r, x = 0; r |= ((b = wasm[i++]) & 127) << x, b >> 7; x += 7) ;
					return r
				};
				type = Get(), len = Get(), iSectionEnd = i + len;
				if (type < 0 || type > 11 || len <= 0 || iSectionEnd > iMax) break;
				//Section 2 'Imports' contains the memory import which describes the initial memory size
				if (type === 2) {
					for (len = Get(), j = 0; j !== len && i < iSectionEnd; j++, (1 === type && Get(1) && Get()), (2 > type && Get()), (3 === type && Get(1)))
						if ((type = Get(Get(Get()))) === 2) {
							// Set the initial heap size and allocate the wasm memory (can be grown with sbrk)
							MEM = obj[fld] = new WebAssembly.Memory({initial: Get(1)});
							i = iSectionEnd = iMax;
						}
				}
			}
		}

		if (kind === 'function') {
			if (obj === env && !env[name]) {
				// First try to find a matching math function, then if the field name matches an aborting call pass a crash function
				// Otherwise pass empty function for things that do nothing in this wasm context (setjmp, __cxa_atexit, __lock, __unlock)
				obj[name] = (Math[name.replace(/^f?([^l].*?)f?$/, '$1').replace(/^rint$/, 'round')]
					|| (name.match(/uncaught_excep|pure_virt|^abort$|^longjmp$/) && (() => crashFunction(name)))
					|| emptyFunction);
				if (env[name] === emptyFunction) console.log("[WASM] Importing empty function for env." + name);
			}
			if (mod.includes('wasi')) {
				// WASI (WebAssembly System Interface) can have varying module names (wasi_unstable/wasi_snapshot_preview1/wasi)
				obj[name] = (name.includes('write') ?
					function (fd, iov, iovcnt, pOutResult) {
						// The fd_write function can only be used to write strings to stdout in this wasm context
						iov >>= 2;
						for (var ret = 0, str = '', i = 0; i < iovcnt; i++) {
							// Process list of IO commands, read passed strings from heap
							const ptr = MU32[iov++];
							const len = MI32[iov++];
							if (len < 0) return -1;
							ret += len;
							str += MStrGet(ptr, len);
							//console.log('fd_write - fd: ' + fd + ' - ['+i+'][len:'+len+']: ' + MStrGet(ptr, len).replace(/\n/g, '\\n'));
						}

						// Print the passed string and write the number of bytes read to the result pointer
						print(str);
						MU32[pOutResult >> 2] = ret;
						return 0; // no error
					}
					// All other IO functions are not emulated so pass empty dummies (fd_read, fd_seek, fd_close)
					: emptyFunction);
			}
		}
	}

	// Store the module reference in WA.wm
	WA.wm = WM = module;

	const instance = await WebAssembly.instantiate(module, imports);

	// Store the list of the functions exported by the wasm module in WA.asm
	WA.asm = ASM = instance.exports;

	const memory = ASM.memory;
	const wasm_call_ctors = ASM.__wasm_call_ctors;
	const main = ASM.main || ASM.__main_argc_argv;
	const mainvoid = ASM.__original_main || ASM.__main_void;
	const malloc = ASM.malloc;

	if (memory) {
		// Get the wasm memory object from the module (can be grown with sbrk)
		MEM = memory;
	}

	if (MEM) {
		// Setup the array memory views and get the initial memory size
		MSetViews();
		WASM_HEAP = MU8.length;
	}

	// If function '__wasm_call_ctors' (global C++ constructors) exists, call it
	if (wasm_call_ctors) wasm_call_ctors();

	// If function 'main' exists, call it
	if (main && malloc) {
		// Allocate 10 bytes of memory to store the argument list with 1 entry to pass to main
		const ptr = malloc(10);

		// Place executable name string "W" after the argv list
		MU8[ptr + 8] = 87;
		MU8[ptr + 9] = 0;

		// argv[0] contains the pointer to the executable name string, argv[1] has a list terminating null pointer
		MU32[(ptr) >> 2] = (ptr + 8)
		MU32[(ptr + 4) >> 2] = 0;

		main(1, ptr);
	} else if (main) {
		// Call the main function with zero arguments
		main(0, 0);
	} else if (mainvoid) {
		// Call the main function without arguments
		mainvoid();
	}
}

export default loadVlowWasm
