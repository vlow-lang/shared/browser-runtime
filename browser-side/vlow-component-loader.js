import loadVlowWasm from "./index.js"

if (document && document.currentScript && document.currentScript.hasAttribute("data-library")) {
	const load = document.currentScript.getAttribute("data-library");
	fetch(load).then(r => r.arrayBuffer()).then(loadVlowWasm)
}
