stages:
  - build
  - upload
  - publish
  - release
  - deploy


variables:
  STANDALONE_RELEASE: "vlow-component-loader.js"
  INTEGRATION_RELEASE: "main.a"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/web-integration"

build_js:
  stage: build
  image: node:15.4
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - browser-side/node_modules/
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
      - browser-side/**/*
  before_script:
    - npm i rollup @rollup/plugin-node-resolve
  script:
    - npm --prefix browser-side/ install
    - |
      cd browser-side/
      npx rollup -c
  artifacts:
    paths:
      - browser-side/dist/

upload_js:
  needs:
    - build_js
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file browser-side/dist/${STANDALONE_RELEASE} ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${STANDALONE_RELEASE}

build_cpp:
  stage: build
  image: registry.gitlab.com/vlow-lang/development-tools/web-integration:latest
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
      - vlow-side/**/*
  script:
    - |
      cd vlow-side
      make
  artifacts:
    paths:
      - vlow-side/dist

upload_cpp:
  needs:
    - build_cpp
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file vlow-side/dist/${INTEGRATION_RELEASE} ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${INTEGRATION_RELEASE}

publish_package:
  stage: publish
  image: node:15.4-alpine
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - |
      {
        echo '@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/'
        echo '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}'
      } >> browser-side/.npmrc
  script:
    - |
      cd browser-side/
      npm publish


release_standalone:
  needs:
    - upload_js
    - upload_cpp
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"${STANDALONE_RELEASE}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${STANDALONE_RELEASE}\"}" \
        --assets-link "{\"name\":\"${INTEGRATION_RELEASE}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${INTEGRATION_RELEASE}\"}"

pages:
  stage: deploy
  image: node:15.4-alpine
  rules:
    - if: $CI_COMMIT_TAG
    - if: '$CI_COMMIT_BRANCH == "master"'
      changes:
        - cdn/**/*
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - cdn/node_modules/
  before_script:
    - apk add --no-cache git curl
  script:
    - mkdir public/
    - |
      git ls-remote origin \
      | cut -f2 \
      | grep 'refs/tags' \
      | cut -d/ -f3 \
      | xargs -I'$' curl -o 'public/$.js' "${PACKAGE_REGISTRY_URL}/$/vlow-component-loader.js";
    - (cd public; cp $(ls -1 | sort -rV | head -1) latest.js)
    - npm --prefix cdn/ install
    - npm --prefix cdn/ run build
    - find public/ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec gzip -f -k {} \;
    # - find . -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k {} \;
  artifacts:
    paths:
      - public
